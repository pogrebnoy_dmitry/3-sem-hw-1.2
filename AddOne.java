import java.util.concurrent.locks.Lock;


public class AddOne implements Runnable {
    private Counter counter;
    private Lock lock;
    private Peterson harry;

    public AddOne(Counter counter, Lock lock, Peterson harry) {
        this.counter = counter;
        this.lock = lock;
        this.harry = harry;
    }
    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            /**
             * Non-lock case
             */
            //counter.incOneCounter();

            /**
             * Standard Java lock case
             */
            /*
            lock.lock();
            counter.incOneCounter();
            lock.unlock();
            */

            /**
             * Peterson algorithm lock case
             */
            harry.lock((int)Thread.currentThread().getId() % 2);
            counter.incOneCounter();
            harry.unlock((int)Thread.currentThread().getId() % 2);
        }
    }
}