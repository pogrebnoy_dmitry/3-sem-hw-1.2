import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Counter count = new Counter(0);
        AddOne[] adder = new AddOne[2];
        Lock lock = new ReentrantLock();
        Peterson harry = new Peterson();
        
        for (int i = 0; i < 2; i++) {
            adder[i] = new AddOne(count, lock, harry);
            Thread thread = new Thread(adder[i]);
            thread.start();
        }
        
        Thread.sleep(100);
        System.out.println("Expected answer " + 2*10000);
        System.out.println("Solved answer " + count.getCounter());
        if (2*10000 == count.getCounter()) {
            System.out.println("Lock is locks!");
        }
    }
}