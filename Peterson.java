public class Peterson {
    /**
     * Implementation of the Peterson algorithm for two threads.
     * Doesn't work on modern architecture!
     */
    private boolean[] wantPoint;
    private int waiting;

    public Peterson () {
        waiting = -1;
        wantPoint = new boolean[2];
        for (int i = 0; i < 2; i++) {
            wantPoint[i] = false;
        }
    }

    void lock(int threadID) {
        int otherThreadID = 1 - threadID;
        wantPoint[threadID] = true;
        waiting = threadID;
        while (wantPoint[otherThreadID] && waiting == threadID) {
            //warm the stone :D
        }
    }

    void unlock(int threadID) {
        wantPoint[threadID] = false;
    }
}